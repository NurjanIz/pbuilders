package com.nur.pbuilders;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PbuildersApplication {

	public static void main(String[] args) {
		SpringApplication.run(PbuildersApplication.class, args);
	}

}
