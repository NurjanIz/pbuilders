package com.nur.pbuilders.resources;

import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.nur.pbuilders.models.Client;
import com.nur.pbuilders.repositories.ClientRepository;

@RestController
@RequestMapping(value = "/clients")
public class ClientResource {

	private ClientRepository clientRepository;

	public ClientResource(ClientRepository clientRepository) {
		super();
		this.clientRepository = clientRepository;
	}

	@PostMapping
	public ResponseEntity<Client> save(@RequestBody Client client) {
		clientRepository.save(client);
		return new ResponseEntity<>(client, HttpStatus.OK);
	}

	@GetMapping
	public ResponseEntity<List<Client>> getAll() {
		List<Client> clients = new ArrayList<>();
		clients = clientRepository.findAll();
		return new ResponseEntity<>(clients, HttpStatus.OK);
	}

	@GetMapping(path = "/pages/{page}")
	public ResponseEntity<Page<Client>> getPage(@PathVariable Integer page) {
		Pageable pageable = PageRequest.of(page, 1);
		Page<Client> clients = clientRepository.findAll(pageable);
		return new ResponseEntity<>(clients, HttpStatus.OK);
	}

	@GetMapping(path = "/{id}")
	public ResponseEntity<Optional<Client>> getById(@PathVariable Integer id) {
		Optional<Client> client;
		try {
			client = clientRepository.findById(id);
			return new ResponseEntity<Optional<Client>>(client, HttpStatus.OK);
		} catch (NoSuchElementException e) {
			return new ResponseEntity<Optional<Client>>(HttpStatus.NOT_FOUND);
		}
	}

	@GetMapping(path = "/nome/{nome}")
	public ResponseEntity<List<Client>> getByNome(@PathVariable String nome) {
		List<Client> clients = new ArrayList<>();
		clients = clientRepository.findByNome(nome);
		return new ResponseEntity<>(clients, HttpStatus.OK);
	}

	@GetMapping(path = "/cpf/{cpf}")
	public ResponseEntity<Optional<Client>> getByCpf(@PathVariable String cpf) {
		Optional<Client> client;
		try {
			client = clientRepository.findByCpf(cpf);
			return new ResponseEntity<Optional<Client>>(client, HttpStatus.OK);
		} catch (NoSuchElementException e) {
			return new ResponseEntity<Optional<Client>>(HttpStatus.NOT_FOUND);
		}
	}

	@GetMapping(path = "/age/{id}")
	public ResponseEntity<Integer> getAge(@PathVariable Integer id) {
		return clientRepository.findById(id).map(client -> {
			return new ResponseEntity<Integer>(Period.between(client.getDataNascimento(), LocalDate.now()).getYears(),
					HttpStatus.OK);
		}).orElse(ResponseEntity.notFound().build());

	}

	@DeleteMapping(path = "/{id}")
	public ResponseEntity<Optional<Client>> deleteById(@PathVariable Integer id) {
		try {
			clientRepository.deleteById(id);
			return new ResponseEntity<Optional<Client>>(HttpStatus.OK);
		} catch (NoSuchElementException e) {
			return new ResponseEntity<Optional<Client>>(HttpStatus.NOT_FOUND);
		}
	}

	@PutMapping(path = "/{id}")
	public ResponseEntity<Client> update(@PathVariable Integer id, @RequestBody Client clientUpdate) {
		return clientRepository.findById(id).map(client -> {
			client.setNome(clientUpdate.getNome());
			client.setCpf(clientUpdate.getCpf());
			client.setDataNascimento(clientUpdate.getDataNascimento());
			Client clientUpdated = clientRepository.save(client);
			return ResponseEntity.ok().body(clientUpdated);
		}).orElse(ResponseEntity.notFound().build());
	}

}
