package com.nur.pbuilders.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.nur.pbuilders.models.Client;

@Repository
public interface ClientRepository extends JpaRepository<Client,Integer>{

	List<Client> findByNome(String nome);

	Optional<Client> findByCpf(String cpf);

}
