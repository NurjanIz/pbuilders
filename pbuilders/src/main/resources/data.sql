DROP TABLE IF EXISTS client;
 
CREATE TABLE client (
  id INT AUTO_INCREMENT  PRIMARY KEY,
  nome VARCHAR(250) NOT NULL,
  cpf VARCHAR(15) NOT NULL,
  data_nascimento DATE
);
 
INSERT INTO client (nome, cpf, data_nascimento) VALUES
  ('First First', '111.111.111-11',  {ts '1990-09-17 18:47:52.69'}),
  ('Second Second', '222.222.222-22',  {ts '1980-03-10 10:17:12.12'});