package com.nur.pbuilders;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.time.LocalDate;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.nur.pbuilders.models.Client;
import com.nur.pbuilders.repositories.ClientRepository;

@SpringBootTest
class PbuildersApplicationTests {

	@Autowired
	private ClientRepository clientRepository;

	@Test
	public void givenClientRepository_whenSaveAndRetreiveEntity_thenOK() {
		Client client = clientRepository.save(new Client("test", "12345", LocalDate.now()));
		Optional<Client> foundClient = clientRepository.findById(client.getId());

		assertNotNull(foundClient.get());
		assertEquals(client.getNome(), foundClient.get().getNome());
	}

}
